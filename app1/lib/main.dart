import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Intento de UberEats',
        theme: ThemeData(
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
              primary: Colors.purple,
            ),
          ),
        ),
        home: MyHomePage());
  }
}

class Loading extends StatefulWidget {
  @override
  _Loading createState() => _Loading();
}

class _Loading extends State<MyHomePage> {
  @override

  Widget build(BuildContext context) {
    return Scaffold(
      body: CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          activeColor: Colors.black,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Cargando'),
            BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Cargando'),
            BottomNavigationBarItem(
                icon: Icon(Icons.receipt), label: 'Cargando'),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Cargando')
          ],
        ),
        tabBuilder: (BuildContext context, int index) {
          return CupertinoTabView(
            builder: (BuildContext context) {
              switch (index) {
                default:
                  return Container();
              }
            },
          );
        },
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override

  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          activeColor: Colors.black,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Inicio'),
            BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Buscar'),
            BottomNavigationBarItem(
                icon: Icon(Icons.receipt), label: 'Pedidos'),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Cuenta')
          ],
        ),
        tabBuilder: (BuildContext context, int index) {
          return CupertinoTabView(
            builder: (BuildContext context) {
              switch (index) {
                case 0:
                  return PageExampleOne();
                  break;
                default:
                  return Container();
              }
            },
          );
        },
      ),
    );
  }
}

class PageExampleOne extends StatefulWidget {
  _PageExampleOne createState() => _PageExampleOne();
}

class _PageExampleOne extends State<PageExampleOne> {
  StreamSubscription<QuerySnapshot> subscription;
  List<DocumentSnapshot> snapshot;
  CollectionReference tiendas = FirebaseFirestore.instance.collection('tiendas');

  @override
  void initState() {
    subscription = tiendas.snapshots().listen((event) {
      setState(() {
        snapshot = event.docs;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      //You should use `Scaffold` if you have `TextField` in body.
      //Otherwise on focus your `TextField` won`t scroll when keyboard popup.
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            //Header Container
            Container(
              padding: const EdgeInsets.all(8.0),
              color: Colors.white,
              height: 120.0,
              alignment: Alignment.center,
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(60.0, 0.0, 60.0, 12.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: DefaultTabController(
                          length: 2,
                          child: TabBar(
                            unselectedLabelColor: Colors.black,
                            indicatorSize: TabBarIndicatorSize.tab,
                            indicator: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.black),
                            tabs: [
                              Tab(text: "A domicilio"),
                              Tab(text: "Para llevar")
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
                    child: Expanded(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                              child: ElevatedButton(
                                onPressed: () {},
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.grey[200],
                                  shape: StadiumBorder(),
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.place,
                                      color: Colors.black,
                                    ),
                                    Text(
                                      "Calle Venecia...",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          5.0, 0.0, 0.0, 0.0),
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            primary: Colors.white,
                                            onPrimary: Colors.black,
                                            shape: StadiumBorder()),
                                        onPressed: () {},
                                        child: Row(
                                          children: <Widget>[
                                            Icon(Icons.access_time_rounded),
                                            Text("Ahora",
                                                style: TextStyle(
                                                    color: Colors.black)),
                                            Icon(Icons.keyboard_arrow_down)
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(0.0, 0.0, 5.0, 0.0),
                            child: IconButton(
                                icon: Icon(Icons.tune), onPressed: () {}),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),

            //Body Container
            Expanded(
              child: SingleChildScrollView(
                padding: const EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10.0),
                      color: Colors.white,
                      height: 170.0,
                      alignment: Alignment.center,
                      child: Expanded(
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                          ),
                          onPressed: () {},
                          child: Expanded(
                            child:
                                Image.asset('assets/1.jpg', fit: BoxFit.fill),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10.0),
                      height: 78.0,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: <Widget>[
                          Container(
                            width: 78.0,
                            color: Colors.white,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.white,
                              ),
                              onPressed: () {},
                              child: Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Image.asset(
                                      'assets/5.jpg',
                                      width: 55,
                                      height: 55,
                                    ),
                                    Text(
                                      "Para llevar",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 9),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: 78.0,
                            color: Colors.white,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.white,
                              ),
                              onPressed: () {},
                              child: Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Image.asset(
                                      'assets/6.jpg',
                                      width: 55,
                                      height: 55,
                                    ),
                                    Text(
                                      "Ofertas",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: 78.0,
                            color: Colors.white,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.white,
                              ),
                              onPressed: () {},
                              child: Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Image.asset(
                                      'assets/7.jpg',
                                      width: 55,
                                      height: 55,
                                    ),
                                    Text(
                                      "Pizza",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: 78.0,
                            color: Colors.white,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.white,
                              ),
                              onPressed: () {},
                              child: Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Image.asset(
                                      'assets/8.jpg',
                                      width: 55,
                                      height: 55,
                                    ),
                                    Text(
                                      "Café y té",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: 78.0,
                            color: Colors.white,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.white,
                              ),
                              onPressed: () {},
                              child: Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Image.asset(
                                      'assets/9.jpg',
                                      width: 55,
                                      height: 55,
                                    ),
                                    Text(
                                      "Americana",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 9),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  Container(
                width: size.width,
                color: Colors.white,
                child: Row(children: <Widget>[
                  Container(
                width: (size.width-10)/2,
                color: Colors.white,
                child: TextButton(
                    onPressed: () {
                      var list = snapshot;
                      list.sort((a, b) {
                        return a
                            .data()['Tiempo']
                            .toString()
                            .toLowerCase()
                            .compareTo(
                                b.data()['Tiempo'].toString().toLowerCase());
                      });
                      setState(() {
                        snapshot = list;
                      });
                    },
                    child: Text('Ordenar por Tiempo')
                )),
                Container(
                width: (size.width-10)/2,
                color: Colors.white,
                child: TextButton(
                    onPressed: () {var list = snapshot;
                      list.sort((a, b) {
                        var a_double = double.parse(a.data()["Precio"]);
                        var b_double = double.parse(b.data()["Precio"]);
                        return a_double.compareTo(b_double);
                      });
                      setState(() {
                        snapshot = list;
                      });},
                    child: Text('Ordenar por Precio')
                )),
                ],
                )),
                  Container(
              width: size.width,
              child: ListView.builder(
                  shrinkWrap: true,
                  padding: EdgeInsets.only(left: 15, right: 15),
                  itemCount: snapshot.length,
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        Stack(
                          children: [
                            Container(
                              width: size.width,
                              height: 160,
                              child: Image.network(
                                snapshot[index].data()["Imagen"],
                                fit: BoxFit.cover,
                              ),
                            ),
                            Positioned(
                              bottom: 15,
                              right: 15,
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.favorite_border_outlined,
                                    color: Colors.white,
                                    size: 20.0,
                                  ),
                                ],
                              ),
                              width: 20,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(snapshot[index].data()["Nombre"],
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w400,
                            )),
                        SizedBox(
                          height: 8,
                        ),
                        Text(snapshot[index].data()["Descripcion"],
                            style: TextStyle(
                              fontSize: 16,
                            )),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          children: [
                            
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[100],
                                  borderRadius: BorderRadius.circular(3)),
                              child: Padding(
                                padding: EdgeInsets.all(5),
                                child: Text("Tiempo " +
                                  snapshot[index].data()["Tiempo"].toString(),
                                  style: TextStyle(
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            SizedBox(
                              width: 26,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[100],
                                  borderRadius: BorderRadius.circular(3)),
                              child: Padding(
                                padding: EdgeInsets.all(5),
                                child: Text(
                                    "\$" +
                                        snapshot[index].data()["Precio"].toString() +
                                        " MXN",
                                    style: TextStyle(
                                      fontSize: 14,
                                    )),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                          ),
                          height: 15,
                          width: size.width,
                        ),
                      ],
                    );
                  }),
            ),

                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
